#include <stdio.h>
#include <string.h>
#include <locale.h>
#define TAM_NOME 50
#define TAM_VECTOR 50

struct musica {
    char titulo[TAM_NOME];
    char artista[TAM_NOME];
    int duracao_min, duracao_seg;
};

void lista_todas_musicas();
void imprime_musica(struct musica * m);
void ler_nova_musica(struct musica * m);
void gravar_musicas_para_ficheiro();
void carregar_musicas_do_ficheiro();

struct musica vec_musicas[TAM_VECTOR]; // vector com musicas
int num_musicas=0; // numero de musicas no vector
char ficheiro_musicas[] = "musicas.txt";

int main() {
    char op;
    do {
        printf("Escolha uma opcao entre as possiveis:\n");
        printf(" [1]Listar musicas\n");
        printf(" [2]Inserir nova musica\n");
        printf(" [3]Gravar musicas para ficheiro\n");
        printf(" [4]Carregar musicas do ficheiro para a memoria\n");
        printf(" [S]air\n");
        fflush(stdin);
        op=getchar();
        switch (op) {
        case '1':
            lista_todas_musicas();
            break;
        case '2':
            ler_nova_musica(&vec_musicas[num_musicas]);
            num_musicas++; // aumenta o numero de musicas no vector
            break;
        case '3':
            gravar_musicas_para_ficheiro();
            break;
        case '4':
            carregar_musicas_do_ficheiro();
            break;
        case 's':
        case 'S':
            break;
        default:
            //system("cls");
            printf("Opcao invalida!!!\n");
        }
        if (op!='s' && op!='S') {
            printf("\n\nprima qualquer tecla para voltar ao menu...");
            getchar();
        }
    }  while (op!='s' && op!='S');

    return 0;
}

void imprime_musica(struct musica * m) {
    printf("-Info sobre a musica\n");
    printf("\tTitulo da musica: %s\n", m->titulo);
    printf("\tArtista: %s\n", m->artista);
    printf("\tDuracao (min:seg): %d:%d\n", m->duracao_min,m->duracao_seg);
}

void ler_nova_musica(struct musica * m) {
    //__fpurge(stdin);
    getchar();
	 printf("Insira o titulo da musica:");
    gets(m->titulo);
    printf("Insira o artista:");
    gets(m->artista);
    printf("Duracao (min:seg):");
    scanf("%d%*c%d",&m->duracao_min,&m->duracao_seg);
}

void lista_todas_musicas() {
    int i;
    printf("----Lista das musicas----\n");
    for (i=0; i<num_musicas; i++)
        imprime_musica(&vec_musicas[i]);
    printf("-------------------------\n");
}

void gravar_musicas_para_ficheiro() {
    FILE * fp;
    int i;
    fp = fopen(ficheiro_musicas, "w");
    if (fp != NULL) {
        fprintf(fp,"musicas: %d\n",num_musicas);
        for (i=0; i<num_musicas; i++) {
            fprintf(fp,"titulo: %s\n", vec_musicas[i].titulo);
            fprintf(fp,"artista: %s\n", vec_musicas[i].artista);
            fprintf(fp,"duracao: %d:%d\n", vec_musicas[i].duracao_min,vec_musicas[i].duracao_seg);
        }
        fclose(fp);
    }
}

void carregar_musicas_do_ficheiro() {
    FILE * fp;
    int i=0;
    char linha[200];
    num_musicas = 0;
    fp = fopen(ficheiro_musicas, "r");
    if (fp != NULL) {
        fscanf(fp,"%*s %d\n", &num_musicas); // ignore the string and store only the int
        for (i=0; i<num_musicas; i++) {
            fgets(linha,200,fp); // titulo
            linha[strlen(linha)-1]=0; // retira quebra de linha
            strcpy(vec_musicas[i].titulo, &linha[8]); // titulo começa no 8º char
            fgets(linha,200,fp); // artista
            linha[strlen(linha)-1]=0; // retira quebra de linha
            strcpy(vec_musicas[i].artista, &linha[9]); // artista começa no 9º char
            fscanf(fp,"%*s %d%*c%d\n", &vec_musicas[i].duracao_min,&vec_musicas[i].duracao_seg); // ignora duração, lê num min, ignora char separador :, lê os segundos
        }
        fclose(fp);
    }
}





